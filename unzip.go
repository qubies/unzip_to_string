package Unzip_To_String

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"path/filepath"
	"sync"
)

type File_Info struct {
	Name string
	Data string
	Err  error
}

func (f File_Info) String() string {
	return fmt.Sprintf("%s\n%s%s\n%s%s\n%s%s\n%s\n",
		"-------------------------------------",
		"Data:", f.Data,
		"FileName:", f.Name,
		"Error:", f.Err,
		"-------------------------------------")
}

type Unzipper struct {
	source_zip string
	Out_Chan   chan *File_Info
	num_threads int
	buffer_size int
}

func New_Unzipper(source_zip string, num_threads int, buffer_size int) *Unzipper {
	U := new(Unzipper)
	U.source_zip = source_zip
	U.Out_Chan = make(chan *File_Info, buffer_size)
	U.num_threads = num_threads
	U.buffer_size = buffer_size
	go U.Unzip()
	return U
}

func (U *Unzipper) handle_zip_file(f *zip.File) {

	this_file := new(File_Info)
	this_file.Err = nil
	if f.FileInfo().IsDir() {
		this_file.Err = errors.New("Is a directory")
		U.Out_Chan <- this_file
		return
	}

	// Open the file
	rc, err := f.Open()
	if err != nil {
		this_file.Err = err
		U.Out_Chan <- this_file
		return
	}
	defer rc.Close()

	// Store filename/path for returning and using later on
	this_file.Name = filepath.Join(U.source_zip, f.Name)

	buffer := new(bytes.Buffer)
	buffer.ReadFrom(rc)
	this_file.Data = buffer.String()

	//success, send the file Data out in string format
	U.Out_Chan <- this_file

}
func (U *Unzipper) handle_file_chan(file_chan chan *zip.File, wg *sync.WaitGroup) {
	for f := range file_chan {
		U.handle_zip_file(f)
	}
	wg.Done()
}
func (U *Unzipper) Unzip() {

	r, err := zip.OpenReader(U.source_zip)
	if err != nil {
		fmt.Println("Error opening file:", U.source_zip)
		return
	}

	defer r.Close()

	wg := new(sync.WaitGroup)
	// loop over all the files in the zip
	file_chan := make(chan *zip.File, U.buffer_size)

	go func() {
		for _, f := range r.File {
			file_chan <- f
		}
		close(file_chan)
	}()

	for x := 0; x < U.num_threads; x++ {
		wg.Add(1)
		go U.handle_file_chan(file_chan, wg)
	}
	wg.Wait()
	close(U.Out_Chan)
}
